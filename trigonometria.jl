# MAC0110 - MiniEP7# Roberto Bolgheroni - 11796430

using Test

# helper functions

function quaseIgual(a, b) # function used to test that a value is very close to b
    error = 0.0001

    if( abs(a - b) <= error )
        return true
    end
    return false
end

function bernoulli(n) # returns the nth bernoulli sequence number, used in tan calculation expression
    n *= 2
    A = Vector{Rational{BigInt}}(undef, n + 1)
    for m = 0 : n
        A[m + 1] = 1 // (m + 1)
        for j = m : -1 : 1
            A[j] = j * (A[j] - A[j + 1])
        end
    end
    return abs(A[1])
end

function factTwoAhead(currentFactorial, current) # used to get factorial 2 numbers ahead of the current
    total = currentFactorial
    for i = (current + 1):(current +2)
        total = BigInt( i * total)
    end
    return total
end

# pt.II test functions

function check_sin(value, xRad)
    return quaseIgual(Base.sin(xRad), value) 
end

function check_cos(value, xRad)
    return quaseIgual(Base.cos(xRad), value) 
end

function check_tan(value, xRad)
    return quaseIgual(Base.tan(xRad), value) 
end

# pt.I functions

function taylor_sin(x)
    seno = BigFloat(0)
    sinal = 1
    i = 1
    div = BigFloat(1)
    while i<=16
        if(i !== 1)
            div = BigFloat(i * (i-1) * (div))
        end
        seno += BigFloat( (sinal) * (x^i) / (div) )
        i+=2
        sinal = sinal * (-1)
    end 
    return seno

end 

function taylor_cos(x)
    cose = BigFloat(1)
    sinal = -1
    i = 2
    div = BigFloat(factorial(2))

    while i<=15
        if(i !== 2)
            div = BigFloat(i * (i-1) * (div))
        end
        cose += BigFloat( (sinal) * (x^i) / (div) )
        i+=2
        sinal = sinal * (-1)
    end 
    return cose

end

function taylor_tan(x)
    tgt = BigFloat(0)
    i = 1
    j = 2
    div = BigFloat(factorial(2))
    # 2^(2 * i) ddn = dois elevado a dois n
    ddn = 4 
    while i<=35
        tgt += BigFloat( (ddn)*(ddn - 1)*bernoulli(i)*(x^(2*i -1)) /(div) )
        i += 1
        div = factTwoAhead(div, j)
        j +=2
        ddn = BigInt(ddn * 4)
    end 
    return tgt

end

# testing section

function test()
    # testing the sin function
    @test check_sin(taylor_sin(0), 0) 
    @test check_sin(taylor_sin(π), π) 
    @test check_sin(taylor_sin(π/2), π/2) 
    @test check_sin(taylor_sin(π/3), π/3) 
    @test check_sin(taylor_sin(π/6), π/6) 
    
    # testing the cos function
    @test check_cos(taylor_cos(0), 0) 
    @test check_cos(taylor_cos(π), π) 
    @test check_cos(taylor_cos(π/2), π/2) 
    @test check_cos(taylor_cos(π/3), π/3) 
    @test check_cos(taylor_cos(π/6), π/6) 
    
    # testing the tan function
    @test check_tan(taylor_tan(π/4), π/4) 
    @test check_tan(taylor_tan(π/6), π/6) 
    @test check_tan(taylor_tan(π/3), π/3) 
    @test check_tan(taylor_tan(3*π/7), 3*π/7) 
    @test check_tan(taylor_tan(4*π/10), 4*π/10)
end

test()